from celery import Celery


app = Celery('celery')
app.config_from_object('celeryconfig')


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
