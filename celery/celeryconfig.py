broker_url = 'redis://redis:6379/0'

task_serializer = 'json'
result_serializer = 'json'
accept_content = ['json']
timezone = 'Europe/London'
enable_utc = True

task_routes = {
    'app.debug_task': 'low-priority',
}
