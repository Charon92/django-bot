import nltk
import requests
import re
import spacy
from . import sentiment_mod as s

from nltk.tokenize import word_tokenize
from spacy import displacy
from collections import Counter
from bs4 import BeautifulSoup

nltk.download('maxent_ne_chunker')
nltk.download('words')

nlp = spacy.load("en_core_web_sm")


def process_text(txt_file):
    raw_text = open(txt_file).read()
    token_text = word_tokenize(raw_text)
    return token_text


def url_to_string(url):
    res = requests.get(url)
    html = res.text
    soup = BeautifulSoup(html, "html5lib")
    for script in soup(['script', 'style', 'aside']):
        script.extract()
    return " ".join(re.split(r'[\n\t]+', soup.get_text()))


def nltk_tagger(token_text):
    tagged_words = nltk.pos_tag(token_text)
    ne_tagged = nltk.ne_chunk(tagged_words)
    return ne_tagged


def structure_ne(ne_tree):
    ne = []
    for subtree in ne_tree:
        if type(subtree) == nltk.Tree:
            ne_label = subtree.label()
            ne_string = " ".join([token for token, pos in subtree.leaves()])
            ne.append((ne_string, ne_label))
    return ne


def nltk_main():
    print(structure_ne(nltk_tagger(process_text('datasets/article.txt'))))


def save_url_to_file(url_data_string):
    f = open('datasets/article.txt', 'w+')
    f.write(url_data_string)
    f.close()


ny_bb = url_to_string(
    'https://medium.com/datadriveninvestor/python-data-science-getting-started-tutorial-nltk-2d8842fedfdd'
)
article = nlp(ny_bb)
len(article.ents)

labels = [x.label_ for x in article.ents]
Counter(labels)

sentences = [x for x in article.sents]
print(sentences[2])

save_url_to_file(ny_bb)
nltk_main()
