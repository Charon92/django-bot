from datetime import datetime
import json
import nltk
import numpy as np
import math
import os
import random
import requests
import string                       # to process standard python strings
import warnings

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

from django.template import loader

SITE_ROOT = os.path.dirname(os.path.realpath(__file__))
WEATHER_API_KEY = 'f71207120684a8019abc531061049566'
NEWS_API_KEY = '15cfafcaaf56447594fc29fe8c7694a8'

warnings.filterwarnings("ignore")

# Get a list of world cities
with open(SITE_ROOT + "/cities.json", "r") as read_file:
    cities = json.load(read_file)

city_list = []

for city_data in cities:
    city_name = city_data['name'].lower()
    city_country_code = city_data['country'].lower()
    city = {'name': city_name,
            'country': city_country_code
            }
    city_list.append(city)


f = open(SITE_ROOT + '/chatbot.txt', 'r', errors='ignore')
raw = f.read()
raw = raw.lower()                       # converts to lowercase
nltk.download('punkt')                  # first-time use only
nltk.download('wordnet')                # first-time use only
sent_tokens = nltk.sent_tokenize(raw)   # converts to list of sentences
word_tokens = nltk.word_tokenize(raw)   # converts to list of words


lemmer = nltk.stem.WordNetLemmatizer()


def lem_tokens(tokens):
    return [lemmer.lemmatize(token) for token in tokens]


remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)


def lem_normalise(text):
    return lem_tokens(nltk.word_tokenize(text.lower().translate(remove_punct_dict)))


GREETING_INPUTS = ("hello", "hi", "greetings", "sup", "what's up", "hey",)
GREETING_RESPONSES = ["hi", "hey", "*nods*", "hi there", "hello", "I am glad! You are talking to me"]

NEWS_INPUTS = ("news", "headlines", "stories")


def get_food_hygiene(query):
    url = "http://api.ratings.food.gov.uk/ratings"
    query = {"q": query}

    headers = {
        "x-api-version": 2
    }

    response = requests.get(url, headers=headers, params=query)
    return response


def news(sentence):
    for word in sentence.split():
        if word.lower() in NEWS_INPUTS:
            return get_news(sentence)
        return None


def get_news(query):
    url = "https://newsapi.org/v2/everything?"
    query = {"q": query}

    headers = {
        'x-api-key': NEWS_API_KEY
    }

    response = requests.get(url, headers=headers, params=query)
    data = response.json()

    ar = data['articles'][0]

    article = {
        'name': ar['source']['name'],
        'title': ar['title'],
        'url' : ar['url'],
        'image': ar['urlToImage']
    }

    template = loader.get_template('article.html')
    response = template.render(article)

    return response


def get_weather(location):
    if location == '':
        location = 'London,GB'
    url = "https://api.openweathermap.org/data/2.5/forecast/hourly?"
    query = {"q": location, "units": "metric"}

    headers = {
        'x-api-key': WEATHER_API_KEY
    }

    response = requests.get(url, headers=headers, params=query)

    data = response.json()
    temperature = data['list'][0]['main']['temp']
    city = location.capitalize()
    weather = data['list'][0]['weather'][0]['description']

    report = "The temperature in {} is currently {}{} , and the forecast is for {}.".format(
        city, temperature, u"\u2103", weather)

    return report


def weather(sentence):

    for word in sentence:
        if word.capitalize() == city['name']:
            return get_weather(word)
        return None


# Checking for greetings
def greeting(sentence):
    """If user's input is a greeting, return a greeting response"""
    for word in sentence.split():
        if word.lower() in GREETING_INPUTS:
            return random.choice(GREETING_RESPONSES)
        else:
            return None


def term_frequency(term, document):
    normalize_document = document.lower().split()
    return normalize_document.count(term.lower()) / float(len(normalize_document))


def inverse_document_frequency(term, all_documents):
    num_docs_with_this_term = 0
    for doc in all_documents:
        if term.lower() in all_documents[doc].lower().split():
            num_docs_with_this_term = num_docs_with_this_term + 1

    if num_docs_with_this_term > 0:
        return 1.0 + math.log(float(len(all_documents)) / num_docs_with_this_term)
    else:
        return 1.0


# Generating response
def response(user_response):
    robo_response = ''
    sent_tokens.append(user_response)
    TfidfVec = TfidfVectorizer(tokenizer=lem_normalise, stop_words='english')
    tfidf = TfidfVec.fit_transform(sent_tokens)
    vals = cosine_similarity(tfidf[-1], tfidf)
    idx = vals.argsort()[0][-2]
    flat = vals.flatten()
    flat.sort()
    req_tfidf = flat[-2]
    if req_tfidf == 0:
        robo_response = robo_response + "I'm sorry, I don't understand you"
        return robo_response
    else:
        robo_response = robo_response + sent_tokens[idx]
        return robo_response


def start():
    statement = "My name is Gary. I can answer all sorts of questions, but I'm a stupid at the minute." \
                " Ask for news by typing 'headlines', 'news' or ask for the weather by typing a location such " \
                "as 'London,GB', 'New York,US', 'Cairo,EG'. I'm working on other things to so try out a normal " \
                "conversation, see what happens. I learn by people talking to me!"
    return statement


def end():
    return "Bye! take care.."


input_mapping = {
    'weather': weather,
    'news': news,
    'thanks': 'You are welcome!',
}


def process(data):
    user_response = data['query']
    user_response.replace(', ', ' ')
    user_response = user_response.lower().split(' ')

    for r in user_response:
        input_mapping[r](user_response)
    return
