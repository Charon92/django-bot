from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import StaticHTMLRenderer
from rest_framework.response import Response
from django.http import JsonResponse

from .robo import *


def home(request):
    return Response('Loaded the homepage')


@api_view(['GET', 'POST'])
@renderer_classes((StaticHTMLRenderer,))
def index(request):
    if request.data != {}:
        data = request.data['query']
    else:
        data = {}

    if request.method == 'GET':
        resp = start()
    elif request.method == 'POST':
        resp = process(data)
    else:
        resp = end()

    return Response(resp)
